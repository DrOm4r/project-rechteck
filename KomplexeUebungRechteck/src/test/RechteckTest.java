package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {

		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);

		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		rechteck5.setX(200);
		rechteck5.setY(200);
		rechteck5.setBreite(200);
		rechteck5.setHoehe(200);

		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
		rechteck6.setX(800);
		rechteck6.setY(400);
		rechteck6.setBreite(20);
		rechteck6.setHoehe(20);

		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);
		rechteck7.setX(800);
		rechteck7.setY(450);
		rechteck7.setBreite(20);
		rechteck7.setHoehe(20);

		Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);
		rechteck8.setX(850);
		rechteck8.setY(400);
		rechteck8.setBreite(20);
		rechteck8.setHoehe(20);

		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);
		rechteck9.setX(855);
		rechteck9.setY(455);
		rechteck9.setBreite(25);
		rechteck9.setHoehe(25);

		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);
		System.out.println(controller.toString());

		Rechteck eck10 = new Rechteck(-4, -5, -50, -200);
		System.out.println(eck10); // Rechteck [x=-4, y=-5, breite=50, hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);// Rechteck [x=-10, y=-10, breite=200, hoehe=100]

	}

	public static void rechteckeTesten() {
		Rechteck RechteckArray[] = new Rechteck[50000];
		Rechteck rechteckgrenze = new Rechteck(0, 0, 1200, 1000);
		int n = 0;
		for (int i = 0; i < 50000; i++) {
			RechteckArray[i] = Rechteck.generiereZufallsRechteck();
			if (rechteckgrenze.enthaelt(RechteckArray[i])) {
				n++;
			}
		}
		if (n == 50000) {
			System.out.println("Alle Rechtecke liegen innerhalb der vorgegebenen Grenzen. " + true);
		} else {
			System.out.println("Nicht alle Rechtecke liegen innerhalb der vorgegebenen Grenzen. " + false);
		}

	}
}
