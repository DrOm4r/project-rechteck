package view;

import javax.swing.JPanel;
import controller.BunteRechteckeController;
import java.awt.Color;
import java.awt.Graphics;

public class Zeichenflaeche extends JPanel {
	private BunteRechteckeController brc;

	public Zeichenflaeche(BunteRechteckeController brc) {
		this.brc = brc;
	}

	public void paintComponent(Graphics g) {
		for (int i = 0; i < brc.getRechtecke().size(); i++) {
			g.setColor(Color.BLACK);
			g.drawRect(brc.getRechtecke().get(i).getX(), brc.getRechtecke().get(i).getY(),
					brc.getRechtecke().get(i).getBreite(), brc.getRechtecke().get(i).getHoehe());
		}
	}
}