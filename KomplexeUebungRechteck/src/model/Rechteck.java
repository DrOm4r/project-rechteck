package model;

import java.util.Random;

public class Rechteck {
	private Punkt p;
	private int breite;
	private int hoehe;

	public Rechteck() {
		this.p = new Punkt(0, 0);
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {

		this.p = new Punkt(x, y);
		this.breite = breite;
		this.hoehe = hoehe;
	}

	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}

	public boolean enthaelt(int x, int y) {
		if (x >= this.p.getX() && y >= this.p.getY() && x <= this.breite && y <= this.hoehe) {
			return true;
		} else {
			return false;
		}
	}

	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}

	public boolean enthaelt(Rechteck rechteck) {

		if (((this.getX() > getX()) && (this.getY() < getY()) && (this.getBreite() > getHoehe())
				&& (this.getHoehe() < getBreite()))) {
			return true;
		} else {
			return false;
		}

	}

	public static Rechteck generiereZufallsRechteck() {
		Random r = new Random();
		int x = r.nextInt(1200);
		int y = r.nextInt(1000);
		Rechteck rechteck = new Rechteck(x, y, r.nextInt(1200 - x), r.nextInt(1000 - y));

		return rechteck;
	}

	@Override
	public String toString() {
		return "Rechteck [p=" + p + ", breite=" + breite + ", hoehe=" + hoehe + ", getX()=" + getX() + ", getY()="
				+ getY() + ", getBreite()=" + getBreite() + ", getHoehe()=" + getHoehe() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
