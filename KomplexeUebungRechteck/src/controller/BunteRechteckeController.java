package controller;

import model.Rechteck;
import java.util.LinkedList;

public class BunteRechteckeController {

	private LinkedList<Rechteck> rechtecke;

	public BunteRechteckeController() {
		this.rechtecke = new LinkedList<Rechteck>();
	}
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}
	public void reset() {
		rechtecke.clear();
	}
	
public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

public void generiereZufallsRechtecke(int anzahl) {
	reset();
	while (anzahl>0) {
		add(Rechteck.generiereZufallsRechteck());
		anzahl--;
	}
}


@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	//	
	public static void main(String[] args) {

	}

}
